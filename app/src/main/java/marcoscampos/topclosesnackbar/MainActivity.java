package marcoscampos.topclosesnackbar;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tx = findViewById(R.id.text);
        tx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testeSnack(getWindow().getDecorView().findViewById(android.R.id.content), getString(R.string.erro), true);
            }
        });
    }

    private void testeSnack(final View view, final String message, boolean indefinite) {

        final TopSnackbar snackbar = TopSnackbar.make(view, message,
                indefinite ? TopSnackbar.LENGTH_INDEFINITE : TopSnackbar.LENGTH_LONG);
        snackbar.show();
    }
}